##
## planet settings
##

#
### source global definitions
#
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

#
### aliases
#

# safety first :)
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

#ls
alias ll='ls -l'
alias la='ls -la'
alias lZ='ls -lZ'
alias l.='ls -d .*'

alias mkdir='mkdir -pv'
alias emacs='emacs -nw'

# fast, integrated and silent way to test for a command
type htop >/dev/null 2>&1 && alias top='htop'

# infos
alias info_mem='free -m -l -t'
# top memory
alias info_psmem='ps auxf | sort -nr -k 4'
alias info_psmem10='ps auxf | sort -nr -k 4 | head -10'
# top cpu
alias info_pscpu='ps auxf | sort -nr -k 3'
alias info_pscpu10='ps auxf | sort -nr -k 3 | head -10'

alias letthemusicplay='mplayer -shuffle *'


# for regularly resetup testmachines
# better use ssh client config
#alias sshn='ssh -o StrictHostKeyChecking=no'


#
### exports
#
# wine env
export WINEARCH=win32
export WINEPREFIX=~/.wine32
#default editor
export EDITOR=zile


#
### functions
#
function sshmux () {
  # try to attach to remote tmux
  $(which ssh) -t $@ "which tmux >/dev/null 2>&1 && (tmux attach || tmux new) || echo \"Sorry tmux is not installed on host $(hostname)\""
}

function .. () {
  # cd up x levels
  levels_up=$1  
  cdarg=""
  for i in $(seq 1 $levels_up); 
    do cdarg="${cdarg}../";
  done
  cd $cdarg
}

function transfer () {
    # upload and share a file via transfer.sh service
    if [ $# -eq 0 ]; then
        echo "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"
        return 1
    fi 
    tmpfile=$( mktemp -t transferXXX )
    if tty -s; then
        basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g')
        curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile
    else
        curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile
    fi
    cat $tmpfile
    rm -f $tmpfile
}


#
### interactive terminal
#
if [ -t 1 ]; then

  ## OS specific settings
  OS=$(uname -s)
  case "$OS" in
    "Linux" )

        # color support for 'less'
        export LESS="--RAW-CONTROL-CHARS"

        # colors for less, man, etc.
        [[ -f ~/.LESS_TERMCAP ]] && . ~/.LESS_TERMCAP

	# deprecated in recent versions
        #export GREP_OPTIONS="--color=auto"
	
	export LS_OPTIONS='--color=auto'

	type dircolors >/dev/null && eval "$(dircolors)" 2>&1

        ;;

    "Darwin"|"FreeBSD")
	
	# note to myself :)
	echo "yeah... it's been a while"
	clear

        # mostly ok
        export CLICOLOR=true

        # color support for 'less'
        export LESS="--RAW-CONTROL-CHARS"

        # use colors for less, man, etc.
        [[ -f ~/.LESS_TERMCAP ]] && . ~/.LESS_TERMCAP

        export GREP_OPTIONS="--color=auto"

        ;;

    * ) 
        echo "no configuration for OS [$OS]"

        ;;

  esac

  #
  ### general interactive terminal settings  
  #
  
  ## bash options

  # check and update window size vars
  shopt -s checkwinsize

  # more history
  export HISTFILESIZE=3000
  # iso8601 an 24h with seconds %Y-%m-%d %H:%M:%S
  export HISTTIMEFORMAT="%F %T"
  # append to history file
  shopt -s histappend	
  # re-edit failed history substitution	
  shopt -s histreedit

  # correct minor path spelling mistakes
  shopt -s cdspell
  # correct dir anmes
  shopt -s dirspell
	
  # try to complete hostnames
  shopt -s hostcomplete

  # no mesg clutter
  type -t mesg > /dev/null 2>&1 && mesg n
  # no biff mail notices
  type -t biff > /dev/null 2>&1 && biff n

  ## bash completion
  if [ -r /etc/bash_completion ]; then
    . /etc/bash_completion
  fi

  ## promt

  # retain X trailing dir components
  export PROMPT_DIRTRIM=4
 
  function genprompt {
    # hmm, static promt instad of generated one on invocation would be nicer

    # tput is generic 
    if type tput >/dev/null 2>&1; then

      # avoid scrambling in dumb terms
      export TERM="${TERM:-dumb}"

      # start non-priting characters
      local snoprint='\['
      # end non-priting characters
      local enoprint='\]'

      # also allow old termcap name
      local bold='\[$(tput bold || tput md)\]' # bold
      local reset='\[$(tput sgr0 || tput me)\]' # reset
      local blink='\[$(tput blink || tput mb)\]' # if you really want to...
      local invert='\[$(tput rev || tput me)\]' # display inverse colors

      # colors - no monomode term(7)
      if [[ -n $(tput colors) ]]; then
	local black='\[$(tput setaf 0 || tput AF 0)\]'
        local red='\[$(tput setaf 1 || tput AF 1)\]'
        local green='\[$(tput setaf 2 || tput AF 2)\]'
        local yellow='\[$(tput setaf 3 || tput AF 3)\]'
        local blue='\[$(tput setaf 4 || tput AF 4)\]'
        local magenta='\[$(tput setaf 5 || tput AF 5)\]'
        local cyan='\[$(tput setaf 6 || tput AF 6)\]'
	local white='\[$(tput setaf 7 || tput AF 7)\]'
      fi

    fi # end tput

    local PLANETROOT=$magenta
    local PLANETUSER=$blue
    local OTHERROOT=$red
    local OTHERUSER=$white

    local PLANET=false
    # are we root?
    if [[ $EUID == 0 ]]; then
      local USERCOLOR=$PLANETROOT
      local ROOTINDICATOR="$USERCOLOR#" 
    else
      if [[ $USER == "daniel" ]]; then
        local PLANET=true
        local USERCOLOR=$PLANETUSER
      else
        local USERCOLOR=$OTHERUSER
      fi
      ROOTINDICATOR="$USERCOLOR:"
    fi

    # some options:
    # hostname - \h
    # CWD honoring PROMPT_DIRTRIM - \w
    # CWD - \W
    # # if euid is 0, $ otherwise - \$ , broken?

    # finally
    export PS1="[${bold}${USERCOLOR}\h${reset}:\w${reset}]${bold}${ROOTINDICATOR}${reset} "
	
  } # end genprompt

genprompt

fi # end interactive terminal

